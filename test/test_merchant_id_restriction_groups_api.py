# coding: utf-8

"""
    Treezor

    Treezor API.  more info [here](https://www.treezor.com).   # noqa: E501

    OpenAPI spec version: 0.1.1
    
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""


from __future__ import absolute_import

import unittest

import swagger_client
from swagger_client.api.merchant_id_restriction_groups_api import MerchantIdRestrictionGroupsApi  # noqa: E501
from swagger_client.rest import ApiException


class TestMerchantIdRestrictionGroupsApi(unittest.TestCase):
    """MerchantIdRestrictionGroupsApi unit test stubs"""

    def setUp(self):
        self.api = swagger_client.api.merchant_id_restriction_groups_api.MerchantIdRestrictionGroupsApi()  # noqa: E501

    def tearDown(self):
        pass

    def test_delete_merchant_id_restriction_groups(self):
        """Test case for delete_merchant_id_restriction_groups

        cancel a merchant id restriction group  # noqa: E501
        """
        pass

    def test_get_merchant_id_restriction_group(self):
        """Test case for get_merchant_id_restriction_group

        search merchant id restriction  groups  # noqa: E501
        """
        pass

    def test_get_merchant_id_restriction_groups_id(self):
        """Test case for get_merchant_id_restriction_groups_id

        get a merchant ID restriction group  # noqa: E501
        """
        pass

    def test_merchant_id_restriction_groups_id_delta_update_put(self):
        """Test case for merchant_id_restriction_groups_id_delta_update_put

        edit the merchant ID list for a restriction group  # noqa: E501
        """
        pass

    def test_merchant_id_restriction_groups_id_presence_check_get(self):
        """Test case for merchant_id_restriction_groups_id_presence_check_get

        check the presence of merchants IDs list for a restriction group  # noqa: E501
        """
        pass

    def test_post_merchant_id_restriction_group(self):
        """Test case for post_merchant_id_restriction_group

        create a merchantIdRestrictionGroup  # noqa: E501
        """
        pass

    def test_put_merchant_id_restriction_groups(self):
        """Test case for put_merchant_id_restriction_groups

        edit a merchant id restriction group (Deprecated)  # noqa: E501
        """
        pass


if __name__ == '__main__':
    unittest.main()
