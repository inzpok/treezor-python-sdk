clean:
	rm -rf swagger_client
	rm -rf python-client
	rm -rf test
	rm -rf docs

generate_client: clean
	curl -X POST -H "Content-Type: application/json" -d '{"swaggerUrl":"https://www.treezor.com/api-documentation/rest-combined.yaml"}' "https://generator.swagger.io/api/gen/clients/python/" -o ./result.json
	python client_generator.py
	sleep 3
	unzip temp.zip -d "./"
	# A changer par votre path car problème de permissions je sais pas pourquoi
	# chmod -R 777 ./treezor-python-sdk
	mv python-client/* .
	rm temp.zip
