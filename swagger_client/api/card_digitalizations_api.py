# coding: utf-8

"""
    Treezor

    Treezor API.  more info [here](https://www.treezor.com).   # noqa: E501

    OpenAPI spec version: 0.1.1
    
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""


from __future__ import absolute_import

import re  # noqa: F401

# python 2 and python 3 compatibility library
import six

from swagger_client.api_client import ApiClient


class CardDigitalizationsApi(object):
    """NOTE: This class is auto generated by the swagger code generator program.

    Do not edit the class manually.
    Ref: https://github.com/swagger-api/swagger-codegen
    """

    def __init__(self, api_client=None):
        if api_client is None:
            api_client = ApiClient()
        self.api_client = api_client

    def card_digitalizations_id(self, id, **kwargs):  # noqa: E501
        """Get a card digitalizations based on its internal id  # noqa: E501

        Get a card digitalizations based on its internal id  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True
        >>> thread = api.card_digitalizations_id(id, async_req=True)
        >>> result = thread.get()

        :param async_req bool
        :param int id: Card digitalization internal id. (required)
        :param str fields: fields to output separated by commas. Possible fields are id, externalId, cardId, detailsFromGPS, status, createdDate, modifiedDate
        :return: object
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs['_return_http_data_only'] = True
        if kwargs.get('async_req'):
            return self.card_digitalizations_id_with_http_info(id, **kwargs)  # noqa: E501
        else:
            (data) = self.card_digitalizations_id_with_http_info(id, **kwargs)  # noqa: E501
            return data

    def card_digitalizations_id_with_http_info(self, id, **kwargs):  # noqa: E501
        """Get a card digitalizations based on its internal id  # noqa: E501

        Get a card digitalizations based on its internal id  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True
        >>> thread = api.card_digitalizations_id_with_http_info(id, async_req=True)
        >>> result = thread.get()

        :param async_req bool
        :param int id: Card digitalization internal id. (required)
        :param str fields: fields to output separated by commas. Possible fields are id, externalId, cardId, detailsFromGPS, status, createdDate, modifiedDate
        :return: object
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = ['id', 'fields']  # noqa: E501
        all_params.append('async_req')
        all_params.append('_return_http_data_only')
        all_params.append('_preload_content')
        all_params.append('_request_timeout')

        params = locals()
        for key, val in six.iteritems(params['kwargs']):
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method card_digitalizations_id" % key
                )
            params[key] = val
        del params['kwargs']
        # verify the required parameter 'id' is set
        if self.api_client.client_side_validation and ('id' not in params or
                                                       params['id'] is None):  # noqa: E501
            raise ValueError("Missing the required parameter `id` when calling `card_digitalizations_id`")  # noqa: E501

        collection_formats = {}

        path_params = {}
        if 'id' in params:
            path_params['id'] = params['id']  # noqa: E501

        query_params = []
        if 'fields' in params:
            query_params.append(('fields', params['fields']))  # noqa: E501

        header_params = {}

        form_params = []
        local_var_files = {}

        body_params = None
        # HTTP header `Accept`
        header_params['Accept'] = self.api_client.select_header_accept(
            ['application/json'])  # noqa: E501

        # HTTP header `Content-Type`
        header_params['Content-Type'] = self.api_client.select_header_content_type(  # noqa: E501
            ['application/json'])  # noqa: E501

        # Authentication setting
        auth_settings = ['api_key']  # noqa: E501

        return self.api_client.call_api(
            '/cardDigitalizations/{id}', 'GET',
            path_params,
            query_params,
            header_params,
            body=body_params,
            post_params=form_params,
            files=local_var_files,
            response_type='object',  # noqa: E501
            auth_settings=auth_settings,
            async_req=params.get('async_req'),
            _return_http_data_only=params.get('_return_http_data_only'),
            _preload_content=params.get('_preload_content', True),
            _request_timeout=params.get('_request_timeout'),
            collection_formats=collection_formats)

    def deletecard_digitalizations_id(self, id, reason_code, **kwargs):  # noqa: E501
        """Deletes a payment Token  # noqa: E501

        Deletes a payment Token  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True
        >>> thread = api.deletecard_digitalizations_id(id, reason_code, async_req=True)
        >>> result = thread.get()

        :param async_req bool
        :param int id: Card digitalization internal id. (required)
        :param str reason_code: The reason code for the action. Possible values are :  | Reason code | Description | | ---- | ----------- | | L | Cardholder confirmed token device lost | | S | Cardholder confirmed token device stolen | | F | Issuer or cardholder confirmed fraudulent token transactions (Deprecated) | | T | Issuer or cardholder confirmed fraudulent token transactions | | C | Account closed | | Z | Other |  (required)
        :return: object
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs['_return_http_data_only'] = True
        if kwargs.get('async_req'):
            return self.deletecard_digitalizations_id_with_http_info(id, reason_code, **kwargs)  # noqa: E501
        else:
            (data) = self.deletecard_digitalizations_id_with_http_info(id, reason_code, **kwargs)  # noqa: E501
            return data

    def deletecard_digitalizations_id_with_http_info(self, id, reason_code, **kwargs):  # noqa: E501
        """Deletes a payment Token  # noqa: E501

        Deletes a payment Token  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True
        >>> thread = api.deletecard_digitalizations_id_with_http_info(id, reason_code, async_req=True)
        >>> result = thread.get()

        :param async_req bool
        :param int id: Card digitalization internal id. (required)
        :param str reason_code: The reason code for the action. Possible values are :  | Reason code | Description | | ---- | ----------- | | L | Cardholder confirmed token device lost | | S | Cardholder confirmed token device stolen | | F | Issuer or cardholder confirmed fraudulent token transactions (Deprecated) | | T | Issuer or cardholder confirmed fraudulent token transactions | | C | Account closed | | Z | Other |  (required)
        :return: object
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = ['id', 'reason_code']  # noqa: E501
        all_params.append('async_req')
        all_params.append('_return_http_data_only')
        all_params.append('_preload_content')
        all_params.append('_request_timeout')

        params = locals()
        for key, val in six.iteritems(params['kwargs']):
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method deletecard_digitalizations_id" % key
                )
            params[key] = val
        del params['kwargs']
        # verify the required parameter 'id' is set
        if self.api_client.client_side_validation and ('id' not in params or
                                                       params['id'] is None):  # noqa: E501
            raise ValueError("Missing the required parameter `id` when calling `deletecard_digitalizations_id`")  # noqa: E501
        # verify the required parameter 'reason_code' is set
        if self.api_client.client_side_validation and ('reason_code' not in params or
                                                       params['reason_code'] is None):  # noqa: E501
            raise ValueError("Missing the required parameter `reason_code` when calling `deletecard_digitalizations_id`")  # noqa: E501

        collection_formats = {}

        path_params = {}
        if 'id' in params:
            path_params['id'] = params['id']  # noqa: E501

        query_params = []
        if 'reason_code' in params:
            query_params.append(('reasonCode', params['reason_code']))  # noqa: E501

        header_params = {}

        form_params = []
        local_var_files = {}

        body_params = None
        # HTTP header `Accept`
        header_params['Accept'] = self.api_client.select_header_accept(
            ['application/json'])  # noqa: E501

        # HTTP header `Content-Type`
        header_params['Content-Type'] = self.api_client.select_header_content_type(  # noqa: E501
            ['application/json'])  # noqa: E501

        # Authentication setting
        auth_settings = ['api_key']  # noqa: E501

        return self.api_client.call_api(
            '/cardDigitalizations/{id}', 'DELETE',
            path_params,
            query_params,
            header_params,
            body=body_params,
            post_params=form_params,
            files=local_var_files,
            response_type='object',  # noqa: E501
            auth_settings=auth_settings,
            async_req=params.get('async_req'),
            _return_http_data_only=params.get('_return_http_data_only'),
            _preload_content=params.get('_preload_content', True),
            _request_timeout=params.get('_request_timeout'),
            collection_formats=collection_formats)

    def putcard_digitalizations_id(self, id, status, reason_code, **kwargs):  # noqa: E501
        """Update the status of a payment Token  # noqa: E501

        Update the status of a payment Token  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True
        >>> thread = api.putcard_digitalizations_id(id, status, reason_code, async_req=True)
        >>> result = thread.get()

        :param async_req bool
        :param int id: Card digitalization internal id. (required)
        :param str status: The new status for the payment Token. Possible values are :  | Status | Description | | ---- | ----------- | | unsuspend | Unsuspend | | suspend | Suspend |  (required)
        :param str reason_code: The reason code for the action. Possible values are : - For a suspension :  | Reason code | Description | | ---- | ----------- | | L | Cardholder confirmed token device lost | | S | Cardholder confirmed token device stolen | | T | Issuer or cardholder confirmed fraudulent token transactions | | Z | Other |  - For an unsuspension :  | Reason code | Description | | ---- | ----------- | | F | Cardholder reported token device found or not stolen | | T | Issuer or cardholder confirmed no fraudulent token transactions | | Z | Other |  (required)
        :return: object
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs['_return_http_data_only'] = True
        if kwargs.get('async_req'):
            return self.putcard_digitalizations_id_with_http_info(id, status, reason_code, **kwargs)  # noqa: E501
        else:
            (data) = self.putcard_digitalizations_id_with_http_info(id, status, reason_code, **kwargs)  # noqa: E501
            return data

    def putcard_digitalizations_id_with_http_info(self, id, status, reason_code, **kwargs):  # noqa: E501
        """Update the status of a payment Token  # noqa: E501

        Update the status of a payment Token  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True
        >>> thread = api.putcard_digitalizations_id_with_http_info(id, status, reason_code, async_req=True)
        >>> result = thread.get()

        :param async_req bool
        :param int id: Card digitalization internal id. (required)
        :param str status: The new status for the payment Token. Possible values are :  | Status | Description | | ---- | ----------- | | unsuspend | Unsuspend | | suspend | Suspend |  (required)
        :param str reason_code: The reason code for the action. Possible values are : - For a suspension :  | Reason code | Description | | ---- | ----------- | | L | Cardholder confirmed token device lost | | S | Cardholder confirmed token device stolen | | T | Issuer or cardholder confirmed fraudulent token transactions | | Z | Other |  - For an unsuspension :  | Reason code | Description | | ---- | ----------- | | F | Cardholder reported token device found or not stolen | | T | Issuer or cardholder confirmed no fraudulent token transactions | | Z | Other |  (required)
        :return: object
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = ['id', 'status', 'reason_code']  # noqa: E501
        all_params.append('async_req')
        all_params.append('_return_http_data_only')
        all_params.append('_preload_content')
        all_params.append('_request_timeout')

        params = locals()
        for key, val in six.iteritems(params['kwargs']):
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method putcard_digitalizations_id" % key
                )
            params[key] = val
        del params['kwargs']
        # verify the required parameter 'id' is set
        if self.api_client.client_side_validation and ('id' not in params or
                                                       params['id'] is None):  # noqa: E501
            raise ValueError("Missing the required parameter `id` when calling `putcard_digitalizations_id`")  # noqa: E501
        # verify the required parameter 'status' is set
        if self.api_client.client_side_validation and ('status' not in params or
                                                       params['status'] is None):  # noqa: E501
            raise ValueError("Missing the required parameter `status` when calling `putcard_digitalizations_id`")  # noqa: E501
        # verify the required parameter 'reason_code' is set
        if self.api_client.client_side_validation and ('reason_code' not in params or
                                                       params['reason_code'] is None):  # noqa: E501
            raise ValueError("Missing the required parameter `reason_code` when calling `putcard_digitalizations_id`")  # noqa: E501

        collection_formats = {}

        path_params = {}
        if 'id' in params:
            path_params['id'] = params['id']  # noqa: E501

        query_params = []
        if 'status' in params:
            query_params.append(('status', params['status']))  # noqa: E501
        if 'reason_code' in params:
            query_params.append(('reasonCode', params['reason_code']))  # noqa: E501

        header_params = {}

        form_params = []
        local_var_files = {}

        body_params = None
        # HTTP header `Accept`
        header_params['Accept'] = self.api_client.select_header_accept(
            ['application/json'])  # noqa: E501

        # HTTP header `Content-Type`
        header_params['Content-Type'] = self.api_client.select_header_content_type(  # noqa: E501
            ['application/json'])  # noqa: E501

        # Authentication setting
        auth_settings = ['api_key']  # noqa: E501

        return self.api_client.call_api(
            '/cardDigitalizations/{id}', 'PUT',
            path_params,
            query_params,
            header_params,
            body=body_params,
            post_params=form_params,
            files=local_var_files,
            response_type='object',  # noqa: E501
            auth_settings=auth_settings,
            async_req=params.get('async_req'),
            _return_http_data_only=params.get('_return_http_data_only'),
            _preload_content=params.get('_preload_content', True),
            _request_timeout=params.get('_request_timeout'),
            collection_formats=collection_formats)

    def readcard_digitalizations(self, **kwargs):  # noqa: E501
        """search for card digitalizations  # noqa: E501

        Search for card digitalizations.  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True
        >>> thread = api.readcard_digitalizations(async_req=True)
        >>> result = thread.get()

        :param async_req bool
        :param str access_signature: Access signature can be mandatory for specific context. Treezor will contact you if so. More info [here](https://agent.treezor.com/security-authentication). 
        :param str access_tag: Access tag is used for idem potency query. More info [here](https://agent.treezor.com/basics). 
        :param int access_user_id: Access user's id is used for user's action restriction. More info [here](https://agent.treezor.com/basics). 
        :param str access_user_ip: Access user's ip is used for user's action restriction. More info [here](https://agent.treezor.com/basics). 
        :param str filter: Filterable fields are : - id - externalId - cardId - status - detailsFromGPS - createdDate - modifiedDate  More info [here](https://agent.treezor.com/filterv2). 
        :param str fields: fields to output separated by commas. Possible fields are id, externalId, cardId, detailsFromGPS, status, createdDate, modifiedDate
        :return: object
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs['_return_http_data_only'] = True
        if kwargs.get('async_req'):
            return self.readcard_digitalizations_with_http_info(**kwargs)  # noqa: E501
        else:
            (data) = self.readcard_digitalizations_with_http_info(**kwargs)  # noqa: E501
            return data

    def readcard_digitalizations_with_http_info(self, **kwargs):  # noqa: E501
        """search for card digitalizations  # noqa: E501

        Search for card digitalizations.  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True
        >>> thread = api.readcard_digitalizations_with_http_info(async_req=True)
        >>> result = thread.get()

        :param async_req bool
        :param str access_signature: Access signature can be mandatory for specific context. Treezor will contact you if so. More info [here](https://agent.treezor.com/security-authentication). 
        :param str access_tag: Access tag is used for idem potency query. More info [here](https://agent.treezor.com/basics). 
        :param int access_user_id: Access user's id is used for user's action restriction. More info [here](https://agent.treezor.com/basics). 
        :param str access_user_ip: Access user's ip is used for user's action restriction. More info [here](https://agent.treezor.com/basics). 
        :param str filter: Filterable fields are : - id - externalId - cardId - status - detailsFromGPS - createdDate - modifiedDate  More info [here](https://agent.treezor.com/filterv2). 
        :param str fields: fields to output separated by commas. Possible fields are id, externalId, cardId, detailsFromGPS, status, createdDate, modifiedDate
        :return: object
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = ['access_signature', 'access_tag', 'access_user_id', 'access_user_ip', 'filter', 'fields']  # noqa: E501
        all_params.append('async_req')
        all_params.append('_return_http_data_only')
        all_params.append('_preload_content')
        all_params.append('_request_timeout')

        params = locals()
        for key, val in six.iteritems(params['kwargs']):
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method readcard_digitalizations" % key
                )
            params[key] = val
        del params['kwargs']

        collection_formats = {}

        path_params = {}

        query_params = []
        if 'access_signature' in params:
            query_params.append(('accessSignature', params['access_signature']))  # noqa: E501
        if 'access_tag' in params:
            query_params.append(('accessTag', params['access_tag']))  # noqa: E501
        if 'access_user_id' in params:
            query_params.append(('accessUserId', params['access_user_id']))  # noqa: E501
        if 'access_user_ip' in params:
            query_params.append(('accessUserIp', params['access_user_ip']))  # noqa: E501
        if 'filter' in params:
            query_params.append(('filter', params['filter']))  # noqa: E501
        if 'fields' in params:
            query_params.append(('fields', params['fields']))  # noqa: E501

        header_params = {}

        form_params = []
        local_var_files = {}

        body_params = None
        # HTTP header `Accept`
        header_params['Accept'] = self.api_client.select_header_accept(
            ['application/json'])  # noqa: E501

        # HTTP header `Content-Type`
        header_params['Content-Type'] = self.api_client.select_header_content_type(  # noqa: E501
            ['application/json'])  # noqa: E501

        # Authentication setting
        auth_settings = ['api_key']  # noqa: E501

        return self.api_client.call_api(
            '/cardDigitalizations', 'GET',
            path_params,
            query_params,
            header_params,
            body=body_params,
            post_params=form_params,
            files=local_var_files,
            response_type='object',  # noqa: E501
            auth_settings=auth_settings,
            async_req=params.get('async_req'),
            _return_http_data_only=params.get('_return_http_data_only'),
            _preload_content=params.get('_preload_content', True),
            _request_timeout=params.get('_request_timeout'),
            collection_formats=collection_formats)
