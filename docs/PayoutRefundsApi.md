# swagger_client.PayoutRefundsApi

All URIs are relative to *https://sandbox.treezor.com/v1/index.php*

Method | HTTP request | Description
------------- | ------------- | -------------
[**get_payout_refunds**](PayoutRefundsApi.md#get_payout_refunds) | **GET** /payoutRefunds | search pay outs refund


# **get_payout_refunds**
> object get_payout_refunds(access_signature=access_signature, access_tag=access_tag, access_user_id=access_user_id, access_user_ip=access_user_ip, id=id, tag=tag, code_status=code_status, information_status=information_status, payout_id=payout_id, page_number=page_number)

search pay outs refund

Get pay out refund that match search criteria.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: api_key
configuration = swagger_client.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.PayoutRefundsApi(swagger_client.ApiClient(configuration))
access_signature = 'access_signature_example' # str | Access signature can be mandatory for specific context. Treezor will contact you if so. More info [here](https://agent.treezor.com/security-authentication). (optional)
access_tag = 'access_tag_example' # str | Access tag is used for idem potency query. More info [here](https://agent.treezor.com/basics). (optional)
access_user_id = 56 # int | Access user's id is used for user's action restriction. More info [here](https://agent.treezor.com/basics). (optional)
access_user_ip = 'access_user_ip_example' # str | Access user's ip is used for user's action restriction. More info [here](https://agent.treezor.com/basics). (optional)
id = 56 # int | PayoutRefund id (optional)
tag = 'tag_example' # str | PayoutRefund tag (optional)
code_status = 'code_status_example' # str | PayoutRefund Code Status (optional)
information_status = 'information_status_example' # str | PayoutRefund Information Status (optional)
payout_id = 56 # int | Payout Id (optional)
page_number = 56 # int | The page number (optional)

try:
    # search pay outs refund
    api_response = api_instance.get_payout_refunds(access_signature=access_signature, access_tag=access_tag, access_user_id=access_user_id, access_user_ip=access_user_ip, id=id, tag=tag, code_status=code_status, information_status=information_status, payout_id=payout_id, page_number=page_number)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling PayoutRefundsApi->get_payout_refunds: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **access_signature** | **str**| Access signature can be mandatory for specific context. Treezor will contact you if so. More info [here](https://agent.treezor.com/security-authentication). | [optional] 
 **access_tag** | **str**| Access tag is used for idem potency query. More info [here](https://agent.treezor.com/basics). | [optional] 
 **access_user_id** | **int**| Access user&#39;s id is used for user&#39;s action restriction. More info [here](https://agent.treezor.com/basics). | [optional] 
 **access_user_ip** | **str**| Access user&#39;s ip is used for user&#39;s action restriction. More info [here](https://agent.treezor.com/basics). | [optional] 
 **id** | **int**| PayoutRefund id | [optional] 
 **tag** | **str**| PayoutRefund tag | [optional] 
 **code_status** | **str**| PayoutRefund Code Status | [optional] 
 **information_status** | **str**| PayoutRefund Information Status | [optional] 
 **payout_id** | **int**| Payout Id | [optional] 
 **page_number** | **int**| The page number | [optional] 

### Return type

**object**

### Authorization

[api_key](../README.md#api_key)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

