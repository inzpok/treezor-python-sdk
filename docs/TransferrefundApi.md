# swagger_client.TransferrefundApi

All URIs are relative to *https://sandbox.treezor.com/v1/index.php*

Method | HTTP request | Description
------------- | ------------- | -------------
[**delete_transferrefund**](TransferrefundApi.md#delete_transferrefund) | **DELETE** /transferrefunds/{id} | cancel a transfer refund
[**get_transferrefund**](TransferrefundApi.md#get_transferrefund) | **GET** /transferrefunds/{id} | get a transfer refund
[**get_transferrefunds**](TransferrefundApi.md#get_transferrefunds) | **GET** /transferrefunds | search transfer refunds
[**post_transferrefunds**](TransferrefundApi.md#post_transferrefunds) | **POST** /transferrefunds | create a transfer refund


# **delete_transferrefund**
> object delete_transferrefund(id)

cancel a transfer refund

Change transfer refund's status to CANCELED. A validated transfer refund can't be cancelled.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: api_key
configuration = swagger_client.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.TransferrefundApi(swagger_client.ApiClient(configuration))
id = 789 # int | Transferrefunds internal id.

try:
    # cancel a transfer refund
    api_response = api_instance.delete_transferrefund(id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling TransferrefundApi->delete_transferrefund: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Transferrefunds internal id. | 

### Return type

**object**

### Authorization

[api_key](../README.md#api_key)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_transferrefund**
> object get_transferrefund(id)

get a transfer refund

Get a transfer refund from the system.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: api_key
configuration = swagger_client.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.TransferrefundApi(swagger_client.ApiClient(configuration))
id = 789 # int | Trasnfert refund internal id.

try:
    # get a transfer refund
    api_response = api_instance.get_transferrefund(id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling TransferrefundApi->get_transferrefund: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Trasnfert refund internal id. | 

### Return type

**object**

### Authorization

[api_key](../README.md#api_key)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_transferrefunds**
> object get_transferrefunds(access_signature=access_signature, access_tag=access_tag, access_user_id=access_user_id, access_user_ip=access_user_ip, transferrefund_id=transferrefund_id, transferrefund_tag=transferrefund_tag, transferrefund_status=transferrefund_status, wallet_id=wallet_id, transfer_id=transfer_id, transferrefund_date=transferrefund_date, user_id=user_id, amount=amount, currency=currency, page_number=page_number, page_count=page_count, sort_by=sort_by, sort_order=sort_order, created_date_from=created_date_from, created_date_to=created_date_to, updated_date_from=updated_date_from, updated_date_to=updated_date_to)

search transfer refunds

Get transfer refunds that match search criteria.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: api_key
configuration = swagger_client.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.TransferrefundApi(swagger_client.ApiClient(configuration))
access_signature = 'access_signature_example' # str | Access signature can be mandatory for specific context. Treezor will contact you if so. More info [here](https://agent.treezor.com/security-authentication).  (optional)
access_tag = 'access_tag_example' # str | Access tag is used for idem potency query. More info [here](https://agent.treezor.com/basics).  (optional)
access_user_id = 56 # int | Access user's id is used for user's action restriction. More info [here](https://agent.treezor.com/basics).  (optional)
access_user_ip = 'access_user_ip_example' # str | Access user's ip is used for user's action restriction. More info [here](https://agent.treezor.com/basics).  (optional)
transferrefund_id = 56 # int | Transfer refund id. (optional)
transferrefund_tag = 'transferrefund_tag_example' # str | Custom data. (optional)
transferrefund_status = 'transferrefund_status_example' # str | Transfer refund status. Possible values: * PENDING * CANCELED * VALIDATED  (optional)
wallet_id = 56 # int | Refunded wallet's id. (optional)
transfer_id = 56 # int | Initial transfer's id. (optional)
transferrefund_date = 56 # int | transfert refund's date. Format: YYYY-MM-DD HH:MM:SS  (optional)
user_id = 56 # int | User's id of who has made the transfer refund. (optional)
amount = 'amount_example' # str | Refund amount. (optional)
currency = 'currency_example' # str | Transfert amount currency. Format: [ISO 4217](https://fr.wikipedia.org/wiki/ISO_4217).  (optional)
page_number = 56 # int | Pagination page number. More info [here](https://agent.treezor.com/lists).  (optional)
page_count = 56 # int | The number of items per page. More info [here](https://agent.treezor.com/lists).  (optional)
sort_by = 'sort_by_example' # str | The transaction element you want to sort the list with. Default value : _createdDate_. More info [here](https://agent.treezor.com/lists).  (optional)
sort_order = 'sort_order_example' # str | The order you want to sort the list. * **DESC** for a descending sort * **ASC** for a ascending sort.  Default value : DESC. More info [here](https://agent.treezor.com/lists).  (optional)
created_date_from = '2013-10-20T19:20:30+01:00' # datetime | The creation date from which you want to filter the request result. Format YYYY-MM-DD HH:MM:SS. More info [here](https://agent.treezor.com/lists)  (optional)
created_date_to = '2013-10-20T19:20:30+01:00' # datetime | The creation date up to which you want to filter the request result. Format YYYY-MM-DD HH:MM:SS. More info [here](https://agent.treezor.com/lists)  (optional)
updated_date_from = '2013-10-20T19:20:30+01:00' # datetime | The modification date from which you want to filter the request result. Format YYYY-MM-DD HH:MM:SS. More info [here](https://agent.treezor.com/lists)  (optional)
updated_date_to = '2013-10-20T19:20:30+01:00' # datetime | The modification date up to which you want to filter the request result. Format YYYY-MM-DD HH:MM:SS. More info [here](https://agent.treezor.com/lists)  (optional)

try:
    # search transfer refunds
    api_response = api_instance.get_transferrefunds(access_signature=access_signature, access_tag=access_tag, access_user_id=access_user_id, access_user_ip=access_user_ip, transferrefund_id=transferrefund_id, transferrefund_tag=transferrefund_tag, transferrefund_status=transferrefund_status, wallet_id=wallet_id, transfer_id=transfer_id, transferrefund_date=transferrefund_date, user_id=user_id, amount=amount, currency=currency, page_number=page_number, page_count=page_count, sort_by=sort_by, sort_order=sort_order, created_date_from=created_date_from, created_date_to=created_date_to, updated_date_from=updated_date_from, updated_date_to=updated_date_to)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling TransferrefundApi->get_transferrefunds: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **access_signature** | **str**| Access signature can be mandatory for specific context. Treezor will contact you if so. More info [here](https://agent.treezor.com/security-authentication).  | [optional] 
 **access_tag** | **str**| Access tag is used for idem potency query. More info [here](https://agent.treezor.com/basics).  | [optional] 
 **access_user_id** | **int**| Access user&#39;s id is used for user&#39;s action restriction. More info [here](https://agent.treezor.com/basics).  | [optional] 
 **access_user_ip** | **str**| Access user&#39;s ip is used for user&#39;s action restriction. More info [here](https://agent.treezor.com/basics).  | [optional] 
 **transferrefund_id** | **int**| Transfer refund id. | [optional] 
 **transferrefund_tag** | **str**| Custom data. | [optional] 
 **transferrefund_status** | **str**| Transfer refund status. Possible values: * PENDING * CANCELED * VALIDATED  | [optional] 
 **wallet_id** | **int**| Refunded wallet&#39;s id. | [optional] 
 **transfer_id** | **int**| Initial transfer&#39;s id. | [optional] 
 **transferrefund_date** | **int**| transfert refund&#39;s date. Format: YYYY-MM-DD HH:MM:SS  | [optional] 
 **user_id** | **int**| User&#39;s id of who has made the transfer refund. | [optional] 
 **amount** | **str**| Refund amount. | [optional] 
 **currency** | **str**| Transfert amount currency. Format: [ISO 4217](https://fr.wikipedia.org/wiki/ISO_4217).  | [optional] 
 **page_number** | **int**| Pagination page number. More info [here](https://agent.treezor.com/lists).  | [optional] 
 **page_count** | **int**| The number of items per page. More info [here](https://agent.treezor.com/lists).  | [optional] 
 **sort_by** | **str**| The transaction element you want to sort the list with. Default value : _createdDate_. More info [here](https://agent.treezor.com/lists).  | [optional] 
 **sort_order** | **str**| The order you want to sort the list. * **DESC** for a descending sort * **ASC** for a ascending sort.  Default value : DESC. More info [here](https://agent.treezor.com/lists).  | [optional] 
 **created_date_from** | **datetime**| The creation date from which you want to filter the request result. Format YYYY-MM-DD HH:MM:SS. More info [here](https://agent.treezor.com/lists)  | [optional] 
 **created_date_to** | **datetime**| The creation date up to which you want to filter the request result. Format YYYY-MM-DD HH:MM:SS. More info [here](https://agent.treezor.com/lists)  | [optional] 
 **updated_date_from** | **datetime**| The modification date from which you want to filter the request result. Format YYYY-MM-DD HH:MM:SS. More info [here](https://agent.treezor.com/lists)  | [optional] 
 **updated_date_to** | **datetime**| The modification date up to which you want to filter the request result. Format YYYY-MM-DD HH:MM:SS. More info [here](https://agent.treezor.com/lists)  | [optional] 

### Return type

**object**

### Authorization

[api_key](../README.md#api_key)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **post_transferrefunds**
> object post_transferrefunds(transfer_id, amount, currency, access_signature=access_signature, access_tag=access_tag, access_user_id=access_user_id, access_user_ip=access_user_ip, transferrefund_tag=transferrefund_tag, comment=comment)

create a transfer refund

Create a new transfer refund in the system.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: api_key
configuration = swagger_client.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.TransferrefundApi(swagger_client.ApiClient(configuration))
transfer_id = 56 # int | transfer's id to refund.
amount = 3.4 # float | Refund amount
currency = 'currency_example' # str | Transfert's currency. Debited wallet and credited wallet must share same currency. Format: [ISO 4217](https://fr.wikipedia.org/wiki/ISO_4217). 
access_signature = 'access_signature_example' # str | Access signature can be mandatory for specific context. Treezor will contact you if so. More info [here](https://agent.treezor.com/security-authentication).  (optional)
access_tag = 'access_tag_example' # str | Access tag is used for idem potency query. More info [here](https://agent.treezor.com/basics).  (optional)
access_user_id = 56 # int | Access user's id is used for user's action restriction. More info [here](https://agent.treezor.com/basics).  (optional)
access_user_ip = 'access_user_ip_example' # str | Access user's ip is used for user's action restriction. More info [here](https://agent.treezor.com/basics).  (optional)
transferrefund_tag = 'transferrefund_tag_example' # str | Custom data. (optional)
comment = 'comment_example' # str | End user, client or issuer comment. (optional)

try:
    # create a transfer refund
    api_response = api_instance.post_transferrefunds(transfer_id, amount, currency, access_signature=access_signature, access_tag=access_tag, access_user_id=access_user_id, access_user_ip=access_user_ip, transferrefund_tag=transferrefund_tag, comment=comment)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling TransferrefundApi->post_transferrefunds: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **transfer_id** | **int**| transfer&#39;s id to refund. | 
 **amount** | **float**| Refund amount | 
 **currency** | **str**| Transfert&#39;s currency. Debited wallet and credited wallet must share same currency. Format: [ISO 4217](https://fr.wikipedia.org/wiki/ISO_4217).  | 
 **access_signature** | **str**| Access signature can be mandatory for specific context. Treezor will contact you if so. More info [here](https://agent.treezor.com/security-authentication).  | [optional] 
 **access_tag** | **str**| Access tag is used for idem potency query. More info [here](https://agent.treezor.com/basics).  | [optional] 
 **access_user_id** | **int**| Access user&#39;s id is used for user&#39;s action restriction. More info [here](https://agent.treezor.com/basics).  | [optional] 
 **access_user_ip** | **str**| Access user&#39;s ip is used for user&#39;s action restriction. More info [here](https://agent.treezor.com/basics).  | [optional] 
 **transferrefund_tag** | **str**| Custom data. | [optional] 
 **comment** | **str**| End user, client or issuer comment. | [optional] 

### Return type

**object**

### Authorization

[api_key](../README.md#api_key)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

