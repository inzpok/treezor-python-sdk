# swagger_client.BusinessApi

All URIs are relative to *https://sandbox.treezor.com/v1/index.php*

Method | HTTP request | Description
------------- | ------------- | -------------
[**businessinformations_get**](BusinessApi.md#businessinformations_get) | **GET** /businessinformations | get business information
[**businesssearchs_get**](BusinessApi.md#businesssearchs_get) | **GET** /businesssearchs | search businesses


# **businessinformations_get**
> object businessinformations_get(country, access_signature=access_signature, access_tag=access_tag, access_user_id=access_user_id, access_user_ip=access_user_ip, external_id=external_id, registration_number=registration_number, vat_number=vat_number)

get business information

The get business information endpoint returns all already known business information. End user will have then to verify/correct it. 

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: api_key
configuration = swagger_client.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.BusinessApi(swagger_client.ApiClient(configuration))
country = 'country_example' # str | In which country to search. Format [ISO 3166-1 alpha-2](https://en.wikipedia.org/wiki/ISO_3166-1_alpha-2) 
access_signature = 'access_signature_example' # str | Access signature can be mandatory for specific context. Treezor will contact you if so. More info [here](https://agent.treezor.com/security-authentication).  (optional)
access_tag = 'access_tag_example' # str | Access tag is used for idem potency query. More info [here](https://agent.treezor.com/basics).  (optional)
access_user_id = 56 # int | Access user's id is used for user's action restriction. More info [here](https://agent.treezor.com/basics).  (optional)
access_user_ip = 'access_user_ip_example' # str | Access user's ip is used for user's action restriction. More info [here](https://agent.treezor.com/basics).  (optional)
external_id = 'external_id_example' # str | External unique business id. As given in businesssearch reponse. (optional)
registration_number = 'registration_number_example' # str | Unique business registration number. (optional)
vat_number = 'vat_number_example' # str | Unique business VAT number. (optional)

try:
    # get business information
    api_response = api_instance.businessinformations_get(country, access_signature=access_signature, access_tag=access_tag, access_user_id=access_user_id, access_user_ip=access_user_ip, external_id=external_id, registration_number=registration_number, vat_number=vat_number)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling BusinessApi->businessinformations_get: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **country** | **str**| In which country to search. Format [ISO 3166-1 alpha-2](https://en.wikipedia.org/wiki/ISO_3166-1_alpha-2)  | 
 **access_signature** | **str**| Access signature can be mandatory for specific context. Treezor will contact you if so. More info [here](https://agent.treezor.com/security-authentication).  | [optional] 
 **access_tag** | **str**| Access tag is used for idem potency query. More info [here](https://agent.treezor.com/basics).  | [optional] 
 **access_user_id** | **int**| Access user&#39;s id is used for user&#39;s action restriction. More info [here](https://agent.treezor.com/basics).  | [optional] 
 **access_user_ip** | **str**| Access user&#39;s ip is used for user&#39;s action restriction. More info [here](https://agent.treezor.com/basics).  | [optional] 
 **external_id** | **str**| External unique business id. As given in businesssearch reponse. | [optional] 
 **registration_number** | **str**| Unique business registration number. | [optional] 
 **vat_number** | **str**| Unique business VAT number. | [optional] 

### Return type

**object**

### Authorization

[api_key](../README.md#api_key)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **businesssearchs_get**
> list[object] businesssearchs_get(country, access_signature=access_signature, access_tag=access_tag, access_user_id=access_user_id, access_user_ip=access_user_ip, name_exact=name_exact, name_match_beginning=name_match_beginning, name_closest_keywords=name_closest_keywords, registration_number=registration_number, vat_number=vat_number, phone_number=phone_number, address_street=address_street, address_city=address_city, address_postal_code=address_postal_code)

search businesses

The search endpoint returns some business information. 

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: api_key
configuration = swagger_client.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.BusinessApi(swagger_client.ApiClient(configuration))
country = 'country_example' # str | In which country to search. Format [ISO 3166-1 alpha-2](https://en.wikipedia.org/wiki/ISO_3166-1_alpha-2) 
access_signature = 'access_signature_example' # str | Access signature can be mandatory for specific context. Treezor will contact you if so. More info [here](https://agent.treezor.com/security-authentication).  (optional)
access_tag = 'access_tag_example' # str | Access tag is used for idem potency query. More info [here](https://agent.treezor.com/basics).  (optional)
access_user_id = 56 # int | Access user's id is used for user's action restriction. More info [here](https://agent.treezor.com/basics).  (optional)
access_user_ip = 'access_user_ip_example' # str | Access user's ip is used for user's action restriction. More info [here](https://agent.treezor.com/basics).  (optional)
name_exact = 'name_exact_example' # str | Business exact name. Exclusive with nameMatchBeginning and nameClosestKeywords.  (optional)
name_match_beginning = 'name_match_beginning_example' # str | Business names begin with. Exclusive with nameExact and nameClosestKeywords.  (optional)
name_closest_keywords = 'name_closest_keywords_example' # str | Business names closed to keywords. Exclusive with nameExact and nameMatchBeginning.  (optional)
registration_number = 'registration_number_example' # str | Business registration number. (optional)
vat_number = 'vat_number_example' # str | Business VAT number. (optional)
phone_number = 'phone_number_example' # str | Business phone number. (optional)
address_street = 'address_street_example' # str | Business' street address. (optional)
address_city = 'address_city_example' # str | Business' city address. (optional)
address_postal_code = 'address_postal_code_example' # str | Business' postal code address. (optional)

try:
    # search businesses
    api_response = api_instance.businesssearchs_get(country, access_signature=access_signature, access_tag=access_tag, access_user_id=access_user_id, access_user_ip=access_user_ip, name_exact=name_exact, name_match_beginning=name_match_beginning, name_closest_keywords=name_closest_keywords, registration_number=registration_number, vat_number=vat_number, phone_number=phone_number, address_street=address_street, address_city=address_city, address_postal_code=address_postal_code)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling BusinessApi->businesssearchs_get: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **country** | **str**| In which country to search. Format [ISO 3166-1 alpha-2](https://en.wikipedia.org/wiki/ISO_3166-1_alpha-2)  | 
 **access_signature** | **str**| Access signature can be mandatory for specific context. Treezor will contact you if so. More info [here](https://agent.treezor.com/security-authentication).  | [optional] 
 **access_tag** | **str**| Access tag is used for idem potency query. More info [here](https://agent.treezor.com/basics).  | [optional] 
 **access_user_id** | **int**| Access user&#39;s id is used for user&#39;s action restriction. More info [here](https://agent.treezor.com/basics).  | [optional] 
 **access_user_ip** | **str**| Access user&#39;s ip is used for user&#39;s action restriction. More info [here](https://agent.treezor.com/basics).  | [optional] 
 **name_exact** | **str**| Business exact name. Exclusive with nameMatchBeginning and nameClosestKeywords.  | [optional] 
 **name_match_beginning** | **str**| Business names begin with. Exclusive with nameExact and nameClosestKeywords.  | [optional] 
 **name_closest_keywords** | **str**| Business names closed to keywords. Exclusive with nameExact and nameMatchBeginning.  | [optional] 
 **registration_number** | **str**| Business registration number. | [optional] 
 **vat_number** | **str**| Business VAT number. | [optional] 
 **phone_number** | **str**| Business phone number. | [optional] 
 **address_street** | **str**| Business&#39; street address. | [optional] 
 **address_city** | **str**| Business&#39; city address. | [optional] 
 **address_postal_code** | **str**| Business&#39; postal code address. | [optional] 

### Return type

**list[object]**

### Authorization

[api_key](../README.md#api_key)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

