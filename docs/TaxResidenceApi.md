# swagger_client.TaxResidenceApi

All URIs are relative to *https://sandbox.treezor.com/v1/index.php*

Method | HTTP request | Description
------------- | ------------- | -------------
[**tax_residences_get**](TaxResidenceApi.md#tax_residences_get) | **GET** /taxResidences | search a residence
[**tax_residences_id_delete**](TaxResidenceApi.md#tax_residences_id_delete) | **DELETE** /taxResidences/{id} | delete a residence
[**tax_residences_id_get**](TaxResidenceApi.md#tax_residences_id_get) | **GET** /taxResidences/{id} | read the informations of a residence
[**tax_residences_id_put**](TaxResidenceApi.md#tax_residences_id_put) | **PUT** /taxResidences/{id} | update the residence
[**tax_residences_post**](TaxResidenceApi.md#tax_residences_post) | **POST** /taxResidences | create a residence


# **tax_residences_get**
> object tax_residences_get(access_signature=access_signature, id=id, user_id=user_id)

search a residence

Search a residence that match search criteria

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: api_key
configuration = swagger_client.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.TaxResidenceApi(swagger_client.ApiClient(configuration))
access_signature = 'access_signature_example' # str | Access signature can be mandatory for specific context. Treezor will  contact you if so. More info  [here](https://agent.treezor.com/security-authentication). (optional)
id = 56 # int | id of the residence (optional)
user_id = 56 # int | User's id residence (optional)

try:
    # search a residence
    api_response = api_instance.tax_residences_get(access_signature=access_signature, id=id, user_id=user_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling TaxResidenceApi->tax_residences_get: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **access_signature** | **str**| Access signature can be mandatory for specific context. Treezor will  contact you if so. More info  [here](https://agent.treezor.com/security-authentication). | [optional] 
 **id** | **int**| id of the residence | [optional] 
 **user_id** | **int**| User&#39;s id residence | [optional] 

### Return type

**object**

### Authorization

[api_key](../README.md#api_key)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **tax_residences_id_delete**
> object tax_residences_id_delete(id, access_signature=access_signature)

delete a residence

Delete a residence that match id

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: api_key
configuration = swagger_client.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.TaxResidenceApi(swagger_client.ApiClient(configuration))
id = 56 # int | id of the residence
access_signature = 'access_signature_example' # str | Access signature can be mandatory for specific context. Treezor will  contact you if so. More info  [here](https://agent.treezor.com/security-authentication). (optional)

try:
    # delete a residence
    api_response = api_instance.tax_residences_id_delete(id, access_signature=access_signature)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling TaxResidenceApi->tax_residences_id_delete: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| id of the residence | 
 **access_signature** | **str**| Access signature can be mandatory for specific context. Treezor will  contact you if so. More info  [here](https://agent.treezor.com/security-authentication). | [optional] 

### Return type

**object**

### Authorization

[api_key](../README.md#api_key)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **tax_residences_id_get**
> object tax_residences_id_get(id, access_signature=access_signature)

read the informations of a residence

Read the informations of a residence that match with id

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: api_key
configuration = swagger_client.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.TaxResidenceApi(swagger_client.ApiClient(configuration))
id = 56 # int | id of the residence
access_signature = 'access_signature_example' # str | Access signature can be mandatory for specific context. Treezor will  contact you if so. More info  [here](https://agent.treezor.com/security-authentication). (optional)

try:
    # read the informations of a residence
    api_response = api_instance.tax_residences_id_get(id, access_signature=access_signature)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling TaxResidenceApi->tax_residences_id_get: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| id of the residence | 
 **access_signature** | **str**| Access signature can be mandatory for specific context. Treezor will  contact you if so. More info  [here](https://agent.treezor.com/security-authentication). | [optional] 

### Return type

**object**

### Authorization

[api_key](../README.md#api_key)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **tax_residences_id_put**
> object tax_residences_id_put(id, access_signature=access_signature, user_id=user_id, country=country, tax_payer_id=tax_payer_id, liability_waiver=liability_waiver)

update the residence

Update a residence already created.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: api_key
configuration = swagger_client.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.TaxResidenceApi(swagger_client.ApiClient(configuration))
id = 56 # int | residence id
access_signature = 'access_signature_example' # str | Access signature can be mandatory for specific context. Treezor will  contact you if so. More info  [here](https://agent.treezor.com/security-authentication). (optional)
user_id = 56 # int | User's id residence (optional)
country = 'country_example' # str | country of the resident (optional)
tax_payer_id = 'tax_payer_id_example' # str | Tax payer's id (optional)
liability_waiver = true # bool | Tax declaration (optional)

try:
    # update the residence
    api_response = api_instance.tax_residences_id_put(id, access_signature=access_signature, user_id=user_id, country=country, tax_payer_id=tax_payer_id, liability_waiver=liability_waiver)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling TaxResidenceApi->tax_residences_id_put: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| residence id | 
 **access_signature** | **str**| Access signature can be mandatory for specific context. Treezor will  contact you if so. More info  [here](https://agent.treezor.com/security-authentication). | [optional] 
 **user_id** | **int**| User&#39;s id residence | [optional] 
 **country** | **str**| country of the resident | [optional] 
 **tax_payer_id** | **str**| Tax payer&#39;s id | [optional] 
 **liability_waiver** | **bool**| Tax declaration | [optional] 

### Return type

**object**

### Authorization

[api_key](../README.md#api_key)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **tax_residences_post**
> object tax_residences_post(user_id, country, access_signature=access_signature, tax_payer_id=tax_payer_id, liability_waiver=liability_waiver)

create a residence

Create a new tax residence.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: api_key
configuration = swagger_client.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.TaxResidenceApi(swagger_client.ApiClient(configuration))
user_id = 56 # int | User's id residence
country = 'country_example' # str | country of the resident
access_signature = 'access_signature_example' # str | Access signature can be mandatory for specific context. Treezor will  contact you if so. More info  [here](https://agent.treezor.com/security-authentication). (optional)
tax_payer_id = 'tax_payer_id_example' # str | Tax payer's id (optional)
liability_waiver = true # bool | When there is no taxPayerId for a residence, this field shall be set to true. (optional)

try:
    # create a residence
    api_response = api_instance.tax_residences_post(user_id, country, access_signature=access_signature, tax_payer_id=tax_payer_id, liability_waiver=liability_waiver)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling TaxResidenceApi->tax_residences_post: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **user_id** | **int**| User&#39;s id residence | 
 **country** | **str**| country of the resident | 
 **access_signature** | **str**| Access signature can be mandatory for specific context. Treezor will  contact you if so. More info  [here](https://agent.treezor.com/security-authentication). | [optional] 
 **tax_payer_id** | **str**| Tax payer&#39;s id | [optional] 
 **liability_waiver** | **bool**| When there is no taxPayerId for a residence, this field shall be set to true. | [optional] 

### Return type

**object**

### Authorization

[api_key](../README.md#api_key)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

