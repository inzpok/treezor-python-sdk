import os
import json
import shutil
from io import BytesIO
from zipfile import ZipFile
from urllib.request import urlopen

with open('result.json') as f:
    data = json.load(f)
    
resp = urlopen(data['link'])

with open('temp.zip', "wb") as f:
    f.write(resp.read())

if os.path.exists('result.json'):
    os.remove('result.json')
    